## Script to get abstracts 
import urllib2 
from bs4 import BeautifulSoup
import nltk
import re

url = "http://www.stanford.edu/class/cs224w/"
page = urllib2.urlopen(url + "handouts.html")
soup = BeautifulSoup(page)
download_links = [] 
links = soup.findAll('a')
if len(links) > 0:
    for link in links:
        if re.search(r'.*pdf$', link['href']):
            if re.search(r'http.*', link['href']):
                download_url = link['href'] # somewhere else
            else:
                download_url = url + link['href'] # in-site
            if link.string == "[Slides]":
                download_links.append((re.sub(".*/", "", download_url), download_url))
            else:
                download_links.append((link.string, download_url))
    for target in download_links:
        print target[0]
        content = urllib2.urlopen(target[1]).read()
        #raw = nltk.clean_html(content)
        #print target
        FILE = open(target[0], "w")
        FILE.write(content)
        FILE.close()
else:
    print "no matches found"