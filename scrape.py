import urlparse
import urllib
from bs4 import BeautifulSoup
import mechanize


#url = 'http://sparkbrowser.com'
#url = 'http://www.mileycyrus.com'

def f1():
	url = 'http://www.nytimes.com'
	urls = [url]
	visited = {}
	visited[url] = 1
	while len(urls) > 0:
		try:
			htmltext = urllib.urlopen(urls[0]).read()
		except:
			print urls[0]
		soup = BeautifulSoup(htmltext)
		urls.pop(0)
		print len(urls)
		for tag in soup.findAll('a',href=True):
			tag['href'] = urlparse.urljoin(url, tag['href'])
			if url in tag['href'] and tag['href'] not in visited:
				urls.append(tag['href'])
				visited[tag['href']] = 1
				print tag['href']
	print visited

def f2():
	url = 'http://adbnews.com/area51/contact.html'
	br = mechanize.Browser()
	br.open(url)
	for link in br.links():
		newurl = urlparse.urljoin(link.base_url, link.url)
		b1 = urlparse.urlparse(newurl).hostname
		b2 = urlparse.urlparse(newurl).path
		print 'http://'+b1+b2
	
if __name__ == '__main__':
	f2()