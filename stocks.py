# author: David Wright
# date: 23 May 2013
# written on Mac OS X, Python 2.7

# pulls stock quotes from yahoo finance, prints to sys.stdout unless redirected
# usage: python stocks.py symbolsfilename

# examples of how to call file:
# python stocks.py symbols.txt
# python stocks.py symbols.txt > quotes.txt

import urllib
import re
import sys
import operator
from threading import Thread
import random
import time

def price_for_symbol_from_yahoo(symbol):
	url = 'http://finance.yahoo.com/q?s='+symbol.lower()+'&ql=1'
	htmlfile = urllib.urlopen(url)
	htmltext = htmlfile.read()
	regex = '<span id="yfs_l84_[^.]*">(.+?)</span>'
	pattern = re.compile(regex)
	price = re.findall(pattern, htmltext)
	if len(price) > 0:
		return float(price[0])
	else:
		return float('nan')

def get_quotes_from_yahoo(symbolsfilename):
	print >> sys.stderr, 'fetching stock prices from yahoo ...'
	symbols = open(symbolsfilename, 'r').readlines()
	symbols = map(lambda x: x.rstrip(), symbols) # remove newlines
	for symbol in symbols:
		print >> sys.stdout, symbol, price_for_symbol_from_yahoo(symbol)
	print >> sys.stderr, 'finished fetching stock prices'

def price_for_symbol_from_yahoo_with_threads(symbol):
	url = 'http://finance.yahoo.com/q?s='+symbol.lower()+'&ql=1'
	htmlfile = urllib.urlopen(url)
	htmltext = htmlfile.read()
	regex = '<span id="yfs_l84_[^.]*">(.+?)</span>'
	pattern = re.compile(regex)
	price = re.findall(pattern, htmltext)
	if len(price) > 0:
		print >> sys.stdout, symbol+' '+price[0]
	else:
		print >> sys.stdout, symbol+' nan'

def get_quotes_from_yahoo_with_threads(symbolsfilename):
	from time import sleep
	symbols = open(symbolsfilename, 'r').readlines()
	symbols = map(lambda x: x.strip(), symbols)
	quotes_dict = {}
	threadlist = []
	for symbol in symbols:
		t = Thread(target=price_for_symbol_from_yahoo_with_threads, args=(symbol,))
		t.start()
		sleep(random.uniform(0.05,0.1)) # otherwise server may block you, thinking you are a crawler, which you are
		threadlist.append(t)
	for t in threadlist:
		t.join()

def price_for_symbol_from_google(symbol):
	url = 'http://www.google.com/finance/q='+symbol.lower()
	htmlfile = urllib.urlopen(url)
	htmltext = htmlfile.read()
	regex = '<span id="ref_[^.]*_1>(.+?)</span>'
	pattern = re.compile(regex)
	price = re.findall(pattern, htmltext)
	if len(price) > 0:
		return float(price[0])
	else:
		return float('nan')

def get_quotes_from_google(symbolsfilename):
	print >> sys.stderr, 'fetching stock prices from google ...'
	symbols = open(symbolsfilename, 'r').readlines()
	symbols = map(lambda x: x.rstrip(), symbols) # remove newlines
	for symbol in symbols:
		print >> sys.stdout, symbol, price_for_symbol_from_yahoo(symbol)
	print >> sys.stderr, 'finished fetching stock prices'

def parse_args():
	if len(sys.argv) == 2:
		symbolsfilename = sys.argv[1]
		return symbolsfilename
	else:
		return None

if __name__ == "__main__":
	symbolsfilename = parse_args()
	if symbolsfilename is not None:
		#get_quotes_from_yahoo(symbolsfilename)
		get_quotes_from_yahoo_with_threads(symbolsfilename)
	else:
		print >> sys.stderr, 'usage: python stocks.py symbolsfilename'
	