# date: 23 May 2013
# written on Mac OS X, Python 2.7

# converts csv file to text, prints to sys.stdout unless redirected
# usage: python csvtosymbols.py csvfilename [column]

# examples of how to call file:
# python csvtosymbols.py csvfile.csv
# python csvtosymbols.py csvfile.csv > symbols.txt
# python csvtosymbols.py csvfile.csv 7
# python stocks.py symbols.txt csvfile.csv 7 > symbols.txt

import sys
import csv

def csv_to_text(csvfilename, col):
	print >> sys.stderr, 'converting csv to txt ...'
	symbolsfromcsv = []
	with open(csvfilename, 'r') as f:
		r = csv.reader(f, delimiter=',')
		next(r) # next skips top row, to avoid header
		for row in r:
			symbolsfromcsv.append(row[col])
	for sym in sorted(symbolsfromcsv):
		print >> sys.stdout, sym
	print >> sys.stderr, 'finished converting csv to txt'

def numcols(csvfilename):
	with open(csvfilename, 'r') as f:
		r = csv.reader(f, delimiter=',')
		return len(next(r))

def parse_args():
	if len(sys.argv) == 2:
		csvfilename = sys.argv[1]
		return csvfilename, 0
	elif len(sys.argv) == 3:
		col = sys.argv[1]
		csvfilename = sys.argv[2]
		return csvfilename, col
	else:
		return None

if __name__ == "__main__":
	csvfilename, col = parse_args()
	if csvfilename is not None:
		csv_to_text(csvfilename, col)
	else:
		print >> sys.stderr, 'usage: python csvtotext.py csvfilename [column]'